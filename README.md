# Helm Chart's in GitLab private registry
This repository contains 2 helm charts:
1. `store-chart`: a helm chart to store in GitLab's private registry
2. `consume-chart`: a helm chart consume the chart from GitLab's private registry.